![HTTP Echo Service](./header.png)

Echoes back the HTTP request. Either call `/json/...` for a JSON view or any other path for a HTML view of the request.

## Usage

### docker-compose.yml

```yaml
version: "2"
services:
  echo-service:
    image: registry.gitlab.com/swinkelhofer/http-echo-service
    ports:
    - "5000:5000"
    environment:
      LISTEN_PORT: "5000"
    # if you need to enable tracing set at least JAEGER_AGENT_HOST
    # for more settings please see https://github.com/jaegertracing/jaeger-client-go
    #  JAEGER_AGENT_HOST: "jaeger-agent"
```

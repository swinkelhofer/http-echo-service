package echo

import (
	"strings"
	"encoding/json"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/uber/jaeger-client-go/config"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"github.com/dgrijalva/jwt-go"
)


// GetEnvDefault reads environment variable and returns fallback if not found
func GetEnvDefault(key string, fallback string) string {
	var (
		ok    bool
		value string
	)
	if value, ok = os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

type JSONResponse struct {
	Headers http.Header
	Method  string
	URI     string
	Proto   string
	Body    string
	JWT 	*jwt.Token	`json:"jwt,omitempty"`
}

func jsonHandler(w http.ResponseWriter, r *http.Request) {
	var (
		err          error
		body         []byte
		span         opentracing.Span
		tracer       opentracing.Tracer
		spanCtx      opentracing.SpanContext
		operation    string
		resp         *JSONResponse
		jsonResponse []byte
		authorization string
	)
	tracer = opentracing.GlobalTracer()

	operation = fmt.Sprintf("HTTP %s %s", r.Method, r.URL.Path)
	spanCtx, _ = tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
	span = tracer.StartSpan(operation, ext.RPCServerOption(spanCtx))
	defer span.Finish()
	span.SetTag("Path", r.URL.EscapedPath())
	span.SetTag("Method", r.Method)
	span.SetTag("Status", "200 OK")

	resp = &JSONResponse{
		Method:  r.Method,
		URI:     r.RequestURI,
		Proto:   r.Proto,
		Headers: r.Header,
	}

	if authorization = r.Header.Get("Authorization"); len(authorization) != 0 {
		resp.JWT = getJWTFromAuthHeader(authorization)
	}

	if r.Method == "POST" || r.Method == "PUT" {
		body, err = ioutil.ReadAll(r.Body)
		if err == nil {
			resp.Body = string(body)
		}
	}

	w.Header().Add("Content-Type", "application/json")
	jsonResponse, err = json.MarshalIndent(resp, "", "    ")

	if err != nil {
		w.WriteHeader(500)
		_, _ = w.Write([]byte(fmt.Sprintf(`{"error": true, "message": "%s"}`, err.Error())))
	} else {
		w.WriteHeader(200)
		_, _ = w.Write(jsonResponse)
	}
}


func getJWTFromAuthHeader(authorization string) *jwt.Token {
	var (
		err    error
		token  *jwt.Token
	)
	authorization = strings.ReplaceAll(authorization, "Bearer ", "")
	if token, err = jwt.Parse(authorization, func(tkn *jwt.Token) (interface{}, error) {
		return nil, nil
	}); err != nil {
		if err.Error() == "key is of invalid type" {
			return token
		}
		return nil
	}
	return token
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	var (
		html         string
		responseBody string
		err          error
		body         []byte
		span         opentracing.Span
		tracer       opentracing.Tracer
		spanCtx      opentracing.SpanContext
		operation    string
		token 		*jwt.Token
		jwtHeader, jwtClaims []byte
	)
	tracer = opentracing.GlobalTracer()

	operation = fmt.Sprintf("HTTP %s %s", r.Method, r.URL.Path)
	spanCtx, _ = tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
	span = tracer.StartSpan(operation, ext.RPCServerOption(spanCtx))
	defer span.Finish()
	span.SetTag("Path", r.URL.EscapedPath())
	span.SetTag("Method", r.Method)
	span.SetTag("Status", "200 OK")

	html = `<html>
        <head>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

        </head>
        <body>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            <div class="jumbotron">
                <div class="container">
                    <h1 class="display-4"><code><i class="fas fa-terminal mr-4" style="
                        text-shadow: 1px 1px #212529;
                    "></i></code>HTTP Echo Service</h1>
                    <p class="lead">This Echo Service simply echoes back your HTTP Request</p>
                </div>
            </div>
            <div class="container">
            %s
            </div>
        </body>
	</html>`
	responseBody = `<h2 class='display-4'>Request</h2>
		<code>` + r.Method + ` ` + r.RequestURI + ` ` + r.Proto + `</code>
		<h2 class='display-4 mt-4'>Request Headers</h2>
		<table class='table'>
			<thead><th width='25%%'>Name</th><th>Value</th></thead>`
	for key, value := range r.Header {
		responseBody += `<tr>
		<td><code>` + key + `</code></td>
		<td><code>`
		for _, h := range value {
			responseBody = responseBody + h + "\n"
		}
		responseBody += `</code>`
		if strings.ToLower(key) == "authorization" {
			if token = getJWTFromAuthHeader(value[0]); token != nil {
				if jwtHeader, err = json.MarshalIndent(token.Header, "", "    "); err != nil {
					continue
				}
				if jwtClaims, err = json.MarshalIndent(token.Claims, "", "    "); err != nil {
					continue
				}
				responseBody += `<div><h4 class="mt-3">JSON Web Token (JWT)</h4>A JWT was detected within the Authorization header. The decoded JWT contents are displayed below.</div>
				<div><h5 class="mt-3">Header</h5><pre><code style="color: #e83e8c">` + string(jwtHeader) + `</code></pre></div>
				<div><h5 class="mt-3">Claims</h5><pre><code style="color: #e83e8c">` + string(jwtClaims) + `</code></pre></div>
				<div><h5 class="mt-3">Signature</h5><code>` + token.Signature + `</code></div>`
			}
		}
		responseBody += `</td>
		</tr>`
		
	}
	responseBody += `</table>`

	if r.Method == "POST" || r.Method == "PUT" {
		body, err = ioutil.ReadAll(r.Body)
		if err == nil {
			responseBody = responseBody + `<h2 class='display-4 mt-4'>Request Body</h2><code>` + string(body) + `</code>`
		}
	}

	_, _ = w.Write([]byte(fmt.Sprintf(html, responseBody)))
}

func setupTracing() {
	var (
		err          error
		jaegerConfig *config.Configuration
		tracer       opentracing.Tracer
	)

	os.Setenv("JAEGER_SERVICE_NAME", "echo-server")

	jaegerConfig, err = config.FromEnv()
	if err != nil {
		log.Fatal(err)
	}

	tracer, _, err = jaegerConfig.NewTracer()
	if err != nil {
		log.Fatal(err)
	}
	opentracing.SetGlobalTracer(tracer)
}

func StartServer() {
	var (
		listenPort string
	)
	setupTracing()
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/json/", jsonHandler)
	listenPort = GetEnvDefault("LISTEN_PORT", "5000")
	log.Fatal(http.ListenAndServe(":"+listenPort, nil))
}

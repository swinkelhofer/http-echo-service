FROM golang:1.14.2-alpine3.11 AS builder

ENV GO111MODULE "on"

COPY . /src
WORKDIR /src
RUN go build -mod readonly gitlab.com/swinkelhofer/http-echo/cmd/http-echo

FROM alpine:3.11
COPY --from=builder /src/http-echo /usr/bin/http-echo

CMD http-echo

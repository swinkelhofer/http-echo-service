package main

import (
	echo "gitlab.com/swinkelhofer/http-echo/internal/http-echo"
)

func main() {
	echo.StartServer()
}
